cmake_minimum_required(VERSION 2.8)
project (XALGUI)

### VERSION ###
set( XALGUI_MAJOR_VERSION 0 )
set( XALGUI_MINOR_VERSION 2 )
set( XALGUI_REVISION_VERSION 3 )

### USER INPUT ###
set( XALGUI_BUILD_SHARED_LIBS true CACHE BOOL "Build shared library." )

### SFML ###
# Detect and add SFML
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
#Find any version 2.X of SFML
#See the FindSFML.cmake file for additional details and instructions
find_package(SFML 2 REQUIRED system window graphics)
if(SFML_FOUND)
	message(STATUS "#1 SFML")
endif()

set( INCLUDE_PATH "${PROJECT_SOURCE_DIR}/include" )
set( SOURCE_PATH "${PROJECT_SOURCE_DIR}/src" )

set(
	SOURCES
	"${INCLUDE_PATH}/XALGUI/Box.hpp"
	"${INCLUDE_PATH}/XALGUI/Button.hpp"
	"${INCLUDE_PATH}/XALGUI/Container.hpp"
	"${INCLUDE_PATH}/XALGUI/Desktop.hpp"
	"${INCLUDE_PATH}/XALGUI/Global.hpp"
	"${INCLUDE_PATH}/XALGUI/HorizontalBox.hpp"
	"${INCLUDE_PATH}/XALGUI/LineEdit.hpp"
	"${INCLUDE_PATH}/XALGUI/MenuBar.hpp"
	"${INCLUDE_PATH}/XALGUI/MenuBarItem.hpp"
	"${INCLUDE_PATH}/XALGUI/MenuSubBar.hpp"
	"${INCLUDE_PATH}/XALGUI/Parser.hpp"
	"${INCLUDE_PATH}/XALGUI/PushButton.hpp"
	"${INCLUDE_PATH}/XALGUI/VerticalBox.hpp"
	"${INCLUDE_PATH}/XALGUI/Widget.hpp"
	"${INCLUDE_PATH}/XALGUI/Window.hpp"
	"${INCLUDE_PATH}/XALGUI/XALGUI.hpp"
	"${SOURCE_PATH}/Desktop.cpp"
	"${SOURCE_PATH}/LineEdit.cpp"
	"${SOURCE_PATH}/MenuBar.cpp"
	"${SOURCE_PATH}/MenuBarItem.cpp"
	"${SOURCE_PATH}/MenuSubBar.cpp"
	"${SOURCE_PATH}/Parser.cpp"
	"${SOURCE_PATH}/PushButton.cpp"
	"${SOURCE_PATH}/Window.cpp"
)

include_directories( ${INCLUDE_PATH} )
include_directories( ${SFML_INCLUDE_DIR} )
include_directories( D:/Programare/Libs/jsoncpp/include )

# Add the library
if( XALGUI_BUILD_SHARED_LIBS )
	# Shared library
	message(STATUS "#1 SHARED")
	add_library(xalgui SHARED ${SOURCES})
	set_target_properties(xalgui PROPERTIES DEBUG_POSTFIX -d)
else()
	# Static library

endif()


# set the output directory for SFML libraries
set(LIBRARY_OUTPUT_PATH "${PROJECT_BINARY_DIR}/lib")

# Platform- and compiler-specific options.
if( WIN32 )
	message(STATUS "#1 WIN32")
	add_definitions( -DWIN32 )
	target_link_libraries( xalgui ${SFML_LIBRARIES} D:/Programare/Libs/jsoncpp/lib/libjsoncpp.a ${SFML_DEPENDENCIES})
	if( CMAKE_COMPILER_IS_GNUCXX )
		if( xalgui_BUILD_SHARED_LIBS )
			set_target_properties( xalgui PROPERTIES PREFIX "" )
		endif()
			set_target_properties( xalgui PROPERTIES IMPORT_SUFFIX ".a" )
		endif()
	set( SHARE_PATH "." )
endif()

if( CMAKE_CXX_COMPILER MATCHES ".*clang[+][+]" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang" )
	set( CMAKE_CXX_FLAGS "-std=c++11 -stdlib=libc++ -Wall -Wextra -Wshadow -Wconversion -Wunused-parameter -Wno-long-long -pedantic ${CMAKE_CXX_FLAGS}" )
	set( CMAKE_C_FLAGS "-Wall -Wextra -Wshadow -Wconversion -Wno-long-long -pedantic ${CMAKE_C_FLAGS}" )
elseif( CMAKE_COMPILER_IS_GNUCXX )
	set( CMAKE_CXX_FLAGS "-std=c++11 -Wall -Wextra -Wshadow -Wconversion -Wno-long-long -pedantic ${CMAKE_CXX_FLAGS}" )
	set( CMAKE_C_FLAGS "-Wall -Wextra -Wshadow -Wconversion -Wno-long-long -pedantic ${CMAKE_C_FLAGS}" )
endif()