#include <XALGUI/Desktop.hpp>

namespace xal
{

Desktop::Desktop()
: m_activeWidget (nullptr)
, m_activeMenuBar (nullptr)
, m_activeWidgetType (-1)
, m_activeObjectContainsMouse (false)
{}

void Desktop::handleEvents(sf::Event &event, sf::Vector2f mousePos)
{
	m_mousePos = mousePos;
	if (m_activeWidgetType == 0) m_activeObjectContainsMouse = objectContainsMousePosition(m_activeWidget);
	else if (m_activeWidgetType == 1) m_activeObjectContainsMouse = objectContainsMousePosition(m_activeMenuBar);

	if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left && !m_activeObjectContainsMouse)
	{
		std::cout << "SEARCHING\n";
		if (m_activeWidgetType == 0)
		{
			m_activeWidget->goPassive();
			m_widgets.push_back(m_activeWidget);
		}
		else if (m_activeWidgetType == 1)
		{
			m_activeMenuBar->goPassive();
			m_menuBars.push_back(m_activeMenuBar);
		}

		for (auto it = m_widgets.begin(); it != m_widgets.end(); ++it)
		{
			if (objectContainsMousePosition(*it))
			{
				m_activeWidget = *it;
				m_widgets.erase(it);
				m_activeWidgetType = 0;
				break;
			}
		}
		for (auto it = m_menuBars.begin(); it != m_menuBars.end(); ++it)
		{
			if (objectContainsMousePosition(*it))
			{
				m_activeMenuBar = *it;
				m_menuBars.erase(it);
				m_activeWidgetType = 1;
				break;
			}
		}
	}

	if (m_activeWidgetType == 0) m_activeWidget->updateEvent(event);
	else if (m_activeWidgetType == 1) m_activeMenuBar->updateEvent(event);
}

void Desktop::update()
{
	updateContainer<Widget::Ptr>(m_widgets);
	updateContainer<MenuBar::Bar::Ptr>(m_menuBars);

	if (m_activeWidgetType == 0) updateActiveObject<Widget::Ptr>(m_activeWidget);
	else if (m_activeWidgetType == 1) updateActiveObject<MenuBar::Bar::Ptr>(m_activeMenuBar);
}

void Desktop::add(MenuBar::Bar::Ptr object)
{
	m_menuBars.push_back(object);
}

void Desktop::add(Widget::Ptr object)
{
	m_widgets.push_back(object);
}

void Desktop::moveToFront(Widget::Ptr object)
{
	auto element = std::find(m_widgets.begin(), m_widgets.end(), object);
	m_widgets.erase(element);
   	m_widgets.push_back(object);
}

void Desktop::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (std::size_t i=0; i<m_widgets.size(); ++i)
	{
		if (m_widgets[i]->isVisible())
		{
			target.draw(*m_widgets[i], states);
		}
	}
	for (std::size_t i=0; i<m_menuBars.size(); ++i)
	{
		if (m_menuBars[i]->isVisible())
		{
			target.draw(*m_menuBars[i], states);
		}
	}
	if (m_activeWidgetType == 0) target.draw(*m_activeWidget, states);
	else if (m_activeWidgetType == 1) target.draw(*m_activeMenuBar, states);
}

bool Desktop::objectContainsMousePosition(MenuBar::Bar::Ptr object)
{
	bool mainObject = sf::FloatRect(object->getPosition().x, object->getPosition().y, object->getSize().x, object->getSize().y).contains(m_mousePos);
	if (mainObject) return true;
	auto sizes = object->getActionBarsGlobalBounds();
	for (std::size_t i=0; i<sizes.size(); ++i)
	{
		if (sizes[i].contains(m_mousePos)) return true;
	}
	return false;
}

bool Desktop::objectContainsMousePosition(Widget::Ptr object)
{
	if (!object) return false;
	//std::cout << "(" << object->getPosition().x << ", " << object->getPosition().y << ", " << object->getSize().x << ", " << object->getSize().y << ") (" << m_mousePos.x << ", " << m_mousePos.y << ")\n";
	return sf::FloatRect(object->getPosition().x, object->getPosition().y, object->getSize().x, object->getSize().y).contains(m_mousePos);
}

}