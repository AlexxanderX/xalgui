#include <XALGUI/MenuSubBar.hpp>
#include <XALGUI/MenuBarItem.hpp>

namespace xal
{

namespace MenuBar
{

SubBar::Ptr SubBar::create(sf::Font &font, unsigned int textSize)
{
	auto ptr = std::make_shared<SubBar>();
	ptr->setFont(font);
	ptr->setTextSize(textSize);
	return ptr;
}

SubBar::SubBar()
: m_itemWasPressed (false)
{
	Box::m_objectsFullSize = false;
	Box::m_useCustomTransformable = true;
}

void SubBar::setFont(sf::Font &font)
{
	m_font = &font;
}

void SubBar::setTextSize(const unsigned int size)
{
	m_textSize = size;
}

void SubBar::setStyle(Parser &parser, std::string name)
{
	if (name != "")
	{
		if (!parser.enterObject(name))
		{
			// ERROR
		}
	}

	if (!parser.getValue("bodyColor").empty())
	{
		m_body.setFillColor(parser.getColor("bodyColor"));
	}

	if (!parser.getValue("borderColor").empty())
	{
		m_body.setOutlineColor(parser.getColor("borderColor"));
	}

	if (!parser.getValue("borderThickness").empty())
	{
		m_body.setOutlineThickness(parser.getValue("borderThickness").asFloat());
	}

	Box::setStyle(parser, "");

	if (name != "")
	{
		parser.exitObject();
	}

	updateSubBar();
}

void SubBar::setStyle(std::string style)
{
	Parser parser(style);
	setStyle(parser, "");
}

std::vector<sf::FloatRect> SubBar::getActionBarsGlobalBounds()
{
	std::vector<sf::FloatRect> sizes;
	for (auto it = m_objects.begin(); it != m_objects.end(); ++it)
	{
		if (it->first->getActionMenuBar() != nullptr && it->first->getActionMenuBar()->isVisible())
		{
			sizes.push_back(it->first->getActionBarGlobalBounds());
			std::vector<sf::FloatRect> additionalSizes = it->first->getActionMenuBar()->getActionBarsGlobalBounds();
			sizes.insert(sizes.end(), additionalSizes.begin(), additionalSizes.end());
			return sizes;
		}
	}

	return sizes;
}

bool SubBar::itemWasPressed()
{
	if (m_itemWasPressed)
	{
		m_itemWasPressed = false;
		return true;
	}
	return false;
}

Item::Ptr SubBar::add(std::string name, std::string style)
{
	auto item = Item::create(*m_font, m_textSize, name);

	if (style != "")
	{
		item->setStyle(Box::m_objectsStyle);
	}
	if (Box::m_objectsStyle != "")
	{
		item->setStyle(Box::m_objectsStyle);
	}

	if (VerticalBox::add(item, name))
	{
		updateSubBar();
	}

	return item;
}

void SubBar::updateObjects()
{
	Box::updateObjects();
	updateSubBar();
}

void SubBar::update(sf::Vector2f mousePos, bool passiveMode)
{
	Box::update(mousePos-getPosition(), passiveMode);
	for (std::size_t i=0; i<m_objects.size(); ++i)
	{
		// Checking if the mouse is pressed on a item to hide others item's menubar
		if (m_objects[i].first->wasButtonActivated())
		{
			m_itemWasPressed = true;
			for (std::size_t j=0; j<m_objects.size(); ++j)
			{
				if (j!=i && m_objects[j].first->getActionMenuBar() != nullptr)
				{
					m_objects[j].first->getActionMenuBar()->setVisible(false);
				}
			}
		}
	}
}

void SubBar::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_body, states);

	Box::draw(target,states);
}

void SubBar::updateSubBar()
{
	if (Box::sizeChanged())
	{
		/* DEBUG */ std::cout << "MenuSubBar():updateSubBar() | sizeChanged!\n";
		m_body.setSize(Box::getSize());
	}
}

}

}