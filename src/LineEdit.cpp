#include <XALGUI/LineEdit.hpp>

namespace xal
{

LineEdit::Ptr LineEdit::create(sf::Font &font, unsigned int characterSize, std::string text)
{
	auto ptr = std::make_shared<LineEdit>();
	ptr->setFont(font);
	ptr->setCharacterSize(characterSize);
	if (!text.empty()) ptr->setText(text);

	return ptr;
}

LineEdit::LineEdit()
: m_font (nullptr)
, m_paddingLeft (0.f)
, m_paddingRight (0.f)
, m_paddingTop (0.f)
, m_paddingBottom (0.f)
, m_mouseInside (false)
, m_mousePressed (false)
, m_cursor (false)
, m_cursorShow (false)
{
	Widget::m_widgetType = "LineEdit";
	Widget::m_fixedSize = {false, true};
}

void LineEdit::setFont(sf::Font &font)
{
	m_font = &font;
	m_text.setFont(font);
	updateLineEdit();
}

void LineEdit::setCharacterSize(unsigned int size)
{
	m_text.setCharacterSize(size);
	updateLineEdit();
}

void LineEdit::setText(std::string text)
{
	m_text.setString(text);
}

void LineEdit::setSize(sf::Vector2f size, bool applyFixedSize, sf::Vector2<bool> fixedSize)
{
	if (applyFixedSize) Widget::m_fixedSize = fixedSize;
	m_body.setSize(size);
	updateLineEdit();
}

void LineEdit::setStyle(Parser &parser, std::string name)
{
	if (name != "")
	{
		if (!parser.enterObject(name))
		{
			// ERROR
		}
	}

	if (!parser.getValue("bodyColor").empty())
	{
		m_body.setFillColor(parser.getColor("bodyColor"));
	}

	if (!parser.getValue("borderColor").empty())
	{
		m_body.setOutlineColor(parser.getColor("borderColor"));
		m_body.setOutlineThickness(1.f);
	}

	if (!parser.getValue("padding").empty())
	{
		float padding = parser.getValue("padding").asFloat();
		m_paddingLeft = padding;
		m_paddingRight = padding;
		m_paddingTop = padding;
		m_paddingBottom = padding;
		m_text.setPosition(m_paddingLeft, m_paddingTop);
	}

	if (!parser.getValue("textColor").empty())
	{
		m_text.setColor(parser.getColor("textColor"));
	}

	if (name != "")
	{
		parser.exitObject();
	}
}

void LineEdit::setStyle(std::string style)
{
	Parser parser(style);
	setStyle(parser, "");
}

sf::Vector2f LineEdit::getSize()
{
	return m_body.getSize();
}

void LineEdit::updateEvent(sf::Event &event)
{
	if (event.type == sf::Event::TextEntered)
	{
		if (event.text.unicode < 128)
		{
			std::string text = m_text.getString().toAnsiString();
			if (event.text.unicode == 8 && !m_text.getString().isEmpty())
			{
				text.pop_back();
			}
			else
			{
				text.push_back(static_cast<char>(event.text.unicode));
			}
			m_text.setString(text);
			m_caret.setPosition(m_text.getPosition().x + m_text.getLocalBounds().width + 0.5f, m_body.getSize().y/2);
		}
	}
	else if (event.type == sf::Event::MouseButtonPressed)
	{
	    if (event.mouseButton.button == sf::Mouse::Left && m_mouseInside)
	    {
	        m_mousePressed = true;
	    }
	}
	else if (event.type == sf::Event::MouseButtonReleased)
	{
		if (event.mouseButton.button == sf::Mouse::Left && m_mouseInside && m_mousePressed)
		{
			m_mousePressed = false;
		 	m_cursor = true;
		 	m_cursorShow = false;

		 	m_caret.setPosition(m_text.getPosition().x + m_text.getLocalBounds().width + 0.5f, m_body.getSize().y/2);

		 	m_clock.restart();		 	
		}
	}
}

void LineEdit::update(sf::Vector2f mousePos, bool passiveMode)
{
	if (m_body.getLocalBounds().contains(mousePos))
	{
		if (passiveMode)
		{
			// Luminite
		}
		else
		{
			m_mouseInside = true;
		}
	}
	else
	{
		m_mouseInside = false;
	}

	if (m_cursor)
	{
		if (m_clock.getElapsedTime().asSeconds() >= 0.75f)
		{
			m_cursorShow = !m_cursorShow;
			m_clock.restart();
		}
	}
}

void LineEdit::goPassive()
{
	if (!m_cursorShow)
	{
		std::string text = m_text.getString().toAnsiString();
		text.pop_back();
		m_text.setString(text);
	}

	m_cursor = false;
}

void LineEdit::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_body, states);
	target.draw(m_text, states);
	if (m_cursorShow) target.draw(m_caret, states);
}

void LineEdit::updateLineEdit()
{
	float bodyHeight = m_paddingTop + getFontHeight(m_text.getCharacterSize()) + m_paddingBottom;
	m_body.setSize({m_body.getSize().x, bodyHeight});
	m_text.setOrigin(m_text.getLocalBounds().left, m_text.getLocalBounds().top);
	m_caret.setSize({1, m_text.getLocalBounds().height + (m_paddingTop/2 + m_paddingBottom/2)});
	m_caret.setOrigin(0.f, m_caret.getSize().y/2);

	Widget::m_minimSize.y = bodyHeight;
	Widget::m_minimSize.x = 10.f;
}

float LineEdit::getFontHeight(unsigned int size)
{
	sf::Text text("QWERTYUIOPASDFGHJKLZXCVBNM", *m_font, size);
	return text.getGlobalBounds().height;
}

}