#include <XALGUI/Parser.hpp>

namespace xal
{

Parser::Parser()
{}

Parser::Parser(std::string style, bool isFilePath)
{
	if (!isFilePath) setStyle(style);
	else setStyleFile(style);
}

void Parser::setStyle(std::string style)
{
	Json::Reader reader;
	bool status = reader.parse(style, m_root, false);
	if (!status)
	{
		// error
	}
}

void Parser::setStyleFile(std::string filePath)
{
	std::ifstream file(filePath);
	if (file.good())
	{
		Json::Reader reader;
		bool status = reader.parse(file, m_root, false);
		if (!status)
		{
			// error
		}
	}
	else
	{
		// ERROR
	}
}

bool Parser::enterObject(std::string name)
{
	if ((m_temporaryValues.empty() && Json::Value(m_root[name]).empty()) || (!m_temporaryValues.empty() && m_temporaryValues.back().get(name, Json::Value()).empty())) return false;

	if (m_temporaryValues.empty())
		m_temporaryValues.push_back(Json::Value(m_root[name]));
	else
		m_temporaryValues.push_back(m_temporaryValues.back().get(name, Json::Value()));

	return true;
}

bool Parser::exitObject()
{
	if (!m_temporaryValues.empty())
	{
		m_temporaryValues.pop_back();
		return true;
	}
	return false;
}

void Parser::exitToRoot()
{
	m_temporaryValues.clear();
}

Json::Value Parser::getValue(std::string name)
{
	if (m_temporaryValues.empty())
		return m_root[name];
	else
		return m_temporaryValues.back().operator[](name);
}

sf::Color Parser::getColor(std::string name)
{
	sf::Color color;

	Json::Value colorData;
	if (!m_temporaryValues.empty())
	{
		colorData = m_temporaryValues.back().operator[](name);
	}
	else colorData = m_root[name];

	color.r = static_cast<unsigned char>(colorData[0u].asInt());
	color.g = static_cast<unsigned char>(colorData[1u].asInt());
	color.b = static_cast<unsigned char>(colorData[2u].asInt());

	if (colorData.size() == 4) color.a = static_cast<unsigned char>(colorData[3u].asInt());

	return color;
}

std::string Parser::getStyle(std::string name)
{
	if (!m_temporaryValues.empty())
	{
		return m_temporaryValues.back().operator[](name).toStyledString();
	}
	return m_root[name].toStyledString();
}

}