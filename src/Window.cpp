#include <XALGUI/Window.hpp>

namespace xal
{

Window::Ptr Window::create(sf::Font &font, std::string title)
{
	Window::Ptr ptr = std::make_shared<Window>();
	ptr->setFont(font);
	ptr->setTitle(title);
	return ptr;
}

Window::Window()
: m_closeButton (nullptr)
, m_moveActive (false)
, m_box (nullptr)
{
	Widget::m_widgetType = "Window";

	m_title.setCharacterSize(30u);
	m_titleBar.setOutlineThickness(1u);
	m_body.setOutlineThickness(1u);
	updateWindow();
}

void Window::setFont(sf::Font &font)
{
	m_font = &font;
	m_title.setFont(*m_font);
	updateWindow();
}

void Window::setTitle(std::string title)
{
	m_title.setString(title);
	updateWindow();
}

void Window::setStyle(Parser &parser, std::string name)
{
	if (name != "")
	{
		if (!parser.enterObject(name))
		{
			// ERROR
		}
	}

	if (!parser.getValue("copy").empty())
	{
		if (name != "")
		{
			parser.exitObject();
		}
		setStyle(parser, parser.getValue("copy").asString());
		if (name != "")
		{
			parser.enterObject(name);
		}
	}

	if (!parser.getValue("bodyColor").empty())
	{
		m_body.setFillColor(parser.getColor("bodyColor"));
	}

	if (!parser.getValue("borderColor").empty())
	{
		m_body.setOutlineColor(parser.getColor("borderColor"));
		m_titleBar.setOutlineColor(m_body.getOutlineColor());
	}

	if (!parser.getValue("titleBarColor").empty())
	{
		m_titleBar.setFillColor(parser.getColor("titleBarColor"));
	}

	if (!parser.getValue("titleColor").empty())
	{
		m_title.setColor(parser.getColor("titleColor"));
	}

	if (!parser.getValue("closeButton").empty())
	{
		m_closeButton = PushButton::create(*m_font, 20, "X");
		m_closeButton->setStyle(parser, "closeButton");
		m_closeButton->setSize({m_closeButton->getSize().y, m_closeButton->getSize().y});
		m_closeButton->setAction(std::bind(&Window::close, this));
		updateWindow();
	}

	if (name != "")
	{
		parser.exitObject();
	}
}

void Window::setStyle(std::string style)
{
	Parser parser(style);
	setStyle(parser, "");
}

void Window::setSize(sf::Vector2f size, bool applyFixedSize, sf::Vector2<bool> fixedSize)
{
	if (applyFixedSize)
	{
		Widget::m_fixedSize = fixedSize;
		if (m_box) m_box->setFixedSize(fixedSize);
	}
	m_body.setSize(size);
	if (m_box) m_box->setSize(size);
	updateWindow();
}

void Window::setBox(Box<Widget::Ptr>::Ptr box)
{
	/* DEBUG */ std::cout << "Window:setBox() | m_box:size = (" << m_body.getSize().x << ", " << m_body.getSize().y << ")\n";

	m_box = box;
	m_box->setPosition(0.f, m_titleBar.getSize().y);
	m_box->setSize(m_body.getSize(), true, Widget::m_fixedSize);
}

sf::Vector2f Window::getSize()
{
	return m_body.getSize() + sf::Vector2f(0.f, m_titleBar.getSize().y);
}

void Window::updateEvent(sf::Event &event)
{
	if (m_box) m_box->updateEvent(event);
	m_closeButton->updateEvent(event);

	if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
	{
		m_moveActive = false;
	}
}

void Window::update(sf::Vector2f mousePos, bool passiveMode)
{
	if (!passiveMode && sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_titleBar.getLocalBounds().contains(mousePos))
	{
		m_moveActive = true;
	}

	if (m_box) m_box->update(mousePos - sf::Vector2f(0.f, m_titleBar.getSize().y), passiveMode);
	m_closeButton->update(mousePos - m_closeButton->getPosition());

	if (m_moveActive)
	{
		mousePos += getPosition();
		if (m_moveOldPos != sf::Vector2f(0.f, 0.f))
		{
			setPosition(getPosition() + mousePos - m_moveOldPos);
		}
		m_moveOldPos = mousePos;
	}
	else
	{
		m_moveOldPos = {0.f, 0.f};
	}
}

void Window::closeAction(std::function<void()> action)
{
	if (m_closeButton) m_closeButton->setAction(action);
}

void Window::close()
{
	Widget::m_deleted = true;
}

void Window::goPassive()
{
	
}

void Window::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_titleBar, states);
	target.draw(m_title, states);
	target.draw(m_body, states);
	target.draw(*m_closeButton, states);

	if (m_box) target.draw(*m_box,states);
}

void Window::updateWindow()
{
	//* DEBUG */ std::cout << "Window():updateWindow()\n";
	m_title.setOrigin(m_title.getLocalBounds().left + m_title.getLocalBounds().width/2, m_title.getLocalBounds().top + m_title.getLocalBounds().height/2);

	// TODO: Optimizez sa se face update doar cand trebuie, mai ales la m_box->setPosition!!!

	float barWidth = m_body.getSize().x;
	//if (barWidth == 0) barWidth = (m_box->getSize().x > m_title.getLocalBounds().width+5.f) ? m_box->getSize().x : m_title.getLocalBounds().width+5.f;
	m_titleBar.setSize(sf::Vector2f(barWidth, m_title.getLocalBounds().height + 5.f));
	m_title.setPosition(m_titleBar.getSize().x/2, m_titleBar.getSize().y/2);

	if (m_box) m_box->setPosition(0.f, m_titleBar.getSize().y);
	m_body.setPosition(0.f, m_titleBar.getSize().y);

	if (m_closeButton != nullptr)
	{
		m_closeButton->setPosition(barWidth-m_closeButton->getSize().x, 0.f);
	}

	//* DEBUG */ std::cout << "\tm_titleBar.getSize(): (" << m_titleBar.getSize().x << ", " << m_titleBar.getSize().y << ")\n";
	//* DEBUG */ std::cout << "\tm_titleBar.getPosition(): (" << m_titleBar.getPosition().x << ", " << m_titleBar.getPosition().y << ")\n";
	//* DEBUG */ std::cout << "\tm_box->getPosition(): (" << m_box->getPosition().x << ", " << m_box->getPosition().y << ")\n";
}

}