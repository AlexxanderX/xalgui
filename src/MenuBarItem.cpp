#include <XALGUI/MenuBarItem.hpp>
#include <XALGUI/MenuSubBar.hpp>

namespace xal
{

namespace MenuBar
{
	
Item::Ptr Item::create(sf::Font &font, uint fontSize, std::string text)
{
	auto ptr = std::make_shared<Item>();
	ptr->setFont(font);
	ptr->setFontSize(fontSize);
	ptr->setText(text);
	return ptr;
}

Item::Item()
: m_actionBar (nullptr)
, m_actionType (ActionType::NoAction)
{
	this->setFixedSize({true,false});
}

void Item::setAction(std::function<void()> action)
{
	PushButton::setAction(action);
	m_actionType = ActionType::Function;
}

void Item::setAction(std::shared_ptr<SubBar> bar)
{
	m_actionIsSet = true;
	m_actionBar = bar;
	updateBar();
}

std::shared_ptr<SubBar> Item::setAction(std::string style)
{
	m_actionIsSet = true;
	m_actionBar = SubBar::create(*m_font, m_fontSize);
	if (style != "") m_actionBar->setStyle(style);
	updateBar();
	return m_actionBar;
}

std::shared_ptr<SubBar> Item::getActionMenuBar()
{
	return m_actionBar;
}

void Item::updateEvent(sf::Event &event)
{
	PushButton::updateEvent(event);

	if (m_actionType == ActionType::MenuBar && m_actionBar->isVisible())
	{
		m_actionBar->updateEvent(event);
	}
}

void Item::update(sf::Vector2f mousePos, bool passiveMode)
{
	PushButton::update(mousePos, passiveMode);

	if (m_actionType == ActionType::MenuBar && m_actionBar->isVisible())
	{
		m_actionBar->update(mousePos, passiveMode);
		if (m_actionBar->itemWasPressed()) m_actionBar->setVisible(false);
	}
}

void Item::setStyle(Parser &parser, std::string name)
{
	PushButton::setStyle(parser, name);
}

void Item::setStyle(std::string style)
{
	PushButton::setStyle(style);
}

void Item::goPassive()
{
	if (m_actionBar) m_actionBar->setVisible(false);
}

sf::FloatRect Item::getActionBarGlobalBounds()
{
	return (m_actionBar && m_actionBar->isVisible()) ? sf::FloatRect(getPosition().x, getPosition().y+m_body.getSize().y, m_actionBar->getSize().x, m_actionBar->getSize().y) : sf::FloatRect(0,0,0,0);
}

void Item::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	PushButton::draw(target, states);

	states.transform *= getTransform();
	if (m_actionType == ActionType::MenuBar && m_actionBar->isVisible())
	{
		target.draw(*m_actionBar, states);
	}
}

void Item::activateAction()
{
	if (m_actionType == ActionType::MenuBar)
 	{ 
 		m_actionBar->setVisible(!m_actionBar->isVisible());
 	}
 	else if (m_actionType == ActionType::Function)
 	{
 		std::cout << "FUNCTION!\n";
 		PushButton::activateAction();
 	}
}

void Item::updateBar()
{
	m_actionType = ActionType::MenuBar;
	m_actionBar->setPosition(0.f, m_body.getSize().y);
	m_actionBar->setVisible(false);
}

}

}