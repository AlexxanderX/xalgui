#include <XALGUI/PushButton.hpp>

namespace xal
{
	
PushButton::Ptr PushButton::create(sf::Font &font, uint fontSize, std::string text)
{
	auto ptr = std::make_shared<PushButton>();
	ptr->setFont(font);
	ptr->setFontSize(fontSize);
	ptr->setText(text);
	return ptr;
}

PushButton::PushButton()
: m_customBodySize (false)
, m_mousePressed (false)
, m_actionDone (false)
, m_mouseInside (false)
, m_readyToSendMouse (true)
, m_actionIsSet (false)
, m_wasButtonActivated (false)
{}

void PushButton::setFont(sf::Font &font)
{
	m_font = &font;
	m_text.setFont(*m_font);
}

void PushButton::setFontSize(uint size)
{
	m_fontSize = size;
	m_text.setCharacterSize(m_fontSize);
	//* DEBUG */ //std::cout << "PushButton():setFontSize():m_fontSize = " << m_fontSize << "\n";
	updatePushButton(true);
}

void PushButton::setText(std::string text)
{
	m_text.setString(text);
	updatePushButton(true);
}

void PushButton::setAction(std::function<void()> action)
{
	m_actionIsSet = true;
	m_action = action;
}

bool PushButton::isMousePressed()
{
	if (m_readyToSendMouse)
	{
		m_readyToSendMouse = false;
		return m_mousePressed;	
	}
	return false;
}

bool PushButton::wasButtonActivated()
{
	if (m_wasButtonActivated)
	{
		m_wasButtonActivated = false;
		return true;
	}
	return false;
}

void PushButton::updateEvent(sf::Event &event)
{
	if (event.type == sf::Event::MouseButtonPressed)
	{
	    if (event.mouseButton.button == sf::Mouse::Left && m_mouseInside)
	    {
	        m_mousePressed = true;
	        m_readyToSendMouse = true;
	        m_body.setFillColor(m_clickedBody);
	        m_body.setOutlineThickness(m_clickedBorderThickness);
	        m_text.setColor(m_clickedText);
	    }
	}
	else if (event.type == sf::Event::MouseButtonReleased)
	{
		 if (event.mouseButton.button == sf::Mouse::Left && m_mouseInside && m_mousePressed)
		 {
		 	m_mousePressed = false;
		 	m_wasButtonActivated = true;
		 	if (m_actionIsSet)
		 		activateAction();
		 }
	}
}

void PushButton::update(sf::Vector2f mousePos, bool passiveMode)
{
	if (m_body.getLocalBounds().contains(mousePos))
	{
		if (!m_mousePressed)
		{
			m_body.setFillColor(m_hoverBody);
			m_body.setOutlineThickness(m_hoverBorderThickness);
		}
		m_mouseInside = true;
	}
	else
	{
		m_body.setFillColor(m_normalBody);
		m_body.setOutlineThickness(m_normalBorderThickness);
		m_mouseInside = false;
	}
}

void PushButton::goPassive()
{
	m_body.setFillColor(m_normalBody);
	m_body.setOutlineThickness(m_normalBorderThickness);
	m_mouseInside = false;
}

void PushButton::setSize(sf::Vector2f size, bool applyFixedSize, sf::Vector2<bool> fixedSize)
{
	if (applyFixedSize) Widget::m_fixedSize = fixedSize;
	m_body.setSize(size);
	m_customBodySize = true;
	//* DEBUG */ std::cout << "PushButton():setSize():size = (" << size.x << ", " << size.y << ")\n";
	updatePushButton();
}

sf::Vector2f PushButton::getSize()
{
	return m_body.getSize();
}


void PushButton::setStyle(Parser &parser, std::string name)
{
	//* DEBUG */ //std::cout << "PushButton():setStyle()\n";
	if (name != "")
	{
		//* DEBUG */ //std::cout << "\tname(string) = " << name << "\n";
		if (!parser.enterObject(name))
		{
			// ERROR
		}
	}

	if (!parser.getValue("normalBodyColor").empty())
	{
		m_normalBody = parser.getColor("normalBodyColor");
		//* DEBUG */ //std::cout << "\tm_normalBody(sf::Color) = (" << int(m_normalBody.r) << ", " << int(m_normalBody.g) << ", " << int(m_normalBody.b) << ")\n"; 
	}
	else
	{
		// error
	}

	if (!parser.getValue("clickedBodyColor").empty())
	{
		m_clickedBody = parser.getColor("clickedBodyColor");
	}
	else
	{
		m_clickedBody = m_normalBody;
	}

	if (!parser.getValue("hoverBodyColor").empty())
	{
		m_hoverBody = parser.getColor("hoverBodyColor");
	}
	else
	{
		m_hoverBody = m_normalBody;
	}

	if (!parser.getValue("normalTextColor").empty())
	{
		m_normalText = parser.getColor("normalTextColor");
	}
	else
	{
		// error
	}

	if (!parser.getValue("clickedTextColor").empty())
	{
		m_clickedText = parser.getColor("clickedTextColor");
	}
	else
	{
		m_clickedText = m_normalText;
	}

	if (!parser.getValue("hoverTextColor").empty())
	{
		m_hoverText = parser.getColor("hoverTextColor");
	}
	else
	{
		m_hoverText = m_normalText;
	}

	if (!parser.getValue("normalBorderColor").empty())
	{
		m_normalBorder = parser.getColor("normalBorderColor");
	}
	if (!parser.getValue("clickedBorderColor").empty())
	{
		m_clickedBorder = parser.getColor("clickedBorderColor");
	}
	if (!parser.getValue("hoverBorderColor").empty())
	{
		m_hoverBorder = parser.getColor("hoverBorderColor");
	}

	if (!parser.getValue("normalBorderThickness").empty())
	{
		m_normalBorderThickness = parser.getValue("normalBorderThickness").asFloat();
	}

	if (!parser.getValue("padding").empty())
	{
		m_padding = parser.getValue("padding").asFloat();
	}

	if (name != "")
	{
		parser.exitObject();
	}

	updatePushButton();
}

void PushButton::setStyle(std::string style)
{
	Parser parser(style);
	setStyle(parser, "");
}

void PushButton::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_body, states);
	target.draw(m_text, states);
}

void PushButton::activateAction()
{
	m_action();
}

void PushButton::updatePushButton(bool changeMinimumSize)
{
	//* DEBUG */ std::cout << "PushButton():updatePushButton()\n";
	m_body.setFillColor(m_normalBody);
	m_body.setOutlineThickness(m_normalBorderThickness);
	if (!m_customBodySize) m_body.setSize(sf::Vector2f(m_padding*2+m_text.getLocalBounds().width, m_padding*2+m_text.getLocalBounds().height));

	// Setting the minim size of the PushButton
	if (changeMinimumSize)
		Widget::m_minimSize = m_body.getSize();
	//* DEBUG */ std::cout << "\tchangeMinimumSize = " << changeMinimumSize << "; m_minimSize = (" << this->m_minimSize.x << ", " << this->m_minimSize.y << ")\n";

	m_text.setColor(m_normalText);
	m_text.setOrigin(m_text.getLocalBounds().left + m_text.getLocalBounds().width/2, m_text.getLocalBounds().top + m_text.getLocalBounds().height/2);
	m_text.setPosition(m_body.getSize().x/2, m_body.getSize().y/2);

	//* DEBUG */ //std::cout << "\tm_body.getSize() = (" << m_body.getSize().x << ", " << m_body.getSize().y << ")\n";
}

}