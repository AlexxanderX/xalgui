#include <XALGUI/MenuBar.hpp>
#include <XALGUI/MenuBarItem.hpp>
#include <XALGUI/MenuSubBar.hpp>

namespace xal
{

namespace MenuBar
{

Bar::Ptr Bar::create(sf::Font &font, unsigned int textSize, float width)
{
	auto ptr = std::make_shared<Bar>();
	ptr->setFont(font);
	ptr->setTextSize(textSize);
	ptr->setSize({width, 0.f});
	return ptr;
}

Bar::Bar()
: m_itemWasPressed (false)
{
	Box::m_useCustomTransformable = true;
}

void Bar::setFont(sf::Font &font)
{
	m_font = &font;
}

void Bar::setTextSize(const unsigned int size)
{
	m_textSize = size;
}

void Bar::setStyle(Parser &parser, std::string name)
{
	if (name != "")
	{
		if (!parser.enterObject(name))
		{
			// ERROR
		}
	}

	if (!parser.getValue("bodyColor").empty())
	{
		m_body.setFillColor(parser.getColor("bodyColor"));
	}

	if (!parser.getValue("borderColor").empty())
	{
		m_body.setOutlineColor(parser.getColor("borderColor"));
	}

	if (!parser.getValue("borderThickness").empty())
	{
		m_body.setOutlineThickness(parser.getValue("borderThickness").asFloat());
	}

	Box::setStyle(parser, "");

	if (name != "")
	{
		parser.exitObject();
	}

	updateBar();
}

void Bar::setStyle(std::string style)
{
	Parser parser(style);
	setStyle(parser, "");
}

std::vector<sf::FloatRect> Bar::getActionBarsGlobalBounds()
{
	std::vector<sf::FloatRect> sizes;
	for (auto it = m_objects.begin(); it != m_objects.end(); ++it)
	{
		if (it->first->getActionMenuBar() != nullptr && it->first->getActionMenuBar()->isVisible())
		{
			sizes.push_back(it->first->getActionBarGlobalBounds());
			std::vector<sf::FloatRect> additionalSizes = it->first->getActionMenuBar()->getActionBarsGlobalBounds();
			sizes.insert(sizes.end(), additionalSizes.begin(), additionalSizes.end());
			return sizes;
		}
	}

	return sizes;
}

bool Bar::itemWasPressed()
{
	if (m_itemWasPressed)
	{
		m_itemWasPressed = false;
		return true;
	}
	return false;
}

std::shared_ptr<Item> Bar::add(std::string name, std::string style)
{
	auto item = Item::create(*m_font, m_textSize, name);
	/* DEBUG */ std::cout << "item is fixed = " << item->isFixedSize().x << "\n";

	if (style != "")
	{
		item->setStyle(Box::m_objectsStyle);
	}
	if (Box::m_objectsStyle != "")
	{
		item->setStyle(Box::m_objectsStyle);
	}

	if (HorizontalBox::add(item, name))
	{
		updateBar();
	}

	return item;
}

void Bar::updateObjects()
{
	Box::updateObjects();
	updateBar();
}

void Bar::update(sf::Vector2f mousePos, bool passiveMode)
{
	Box::update(mousePos-getPosition(), passiveMode);
	for (std::size_t i=0; i<m_objects.size(); ++i)
	{
		// Checking if the mouse is pressed on a item to hide others item's menubar
		if (m_objects[i].first->wasButtonActivated())
		{
			m_itemWasPressed = true;
			for (std::size_t j=0; j<m_objects.size(); ++j)
			{
				if (j!=i && m_objects[j].first->getActionMenuBar() != nullptr)
				{
					m_objects[j].first->getActionMenuBar()->setVisible(false);
				}
			}
		}
	}
	//* DEBUG */ std::cout << m_body.getSize().x << " " << m_body.getSize().y << "\n";
}

void Bar::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_body, states);

	Box::draw(target,states);
}

void Bar::updateBar()
{
	if (Box::sizeChanged())
	{
		m_body.setSize(Box::getSize());
	}
}

}

}