#ifndef XAL_PUSHBUTTON_HPP
#define XAL_PUSHBUTTON_HPP

#include <SFML/Graphics.hpp>

#include <XALGUI/Button.hpp>

namespace xal
{

class PushButton: public Button
{
public:
	typedef std::shared_ptr<PushButton> Ptr;

	static Ptr create(sf::Font &font, uint fontSize, std::string text);
	PushButton();

	void setFont(sf::Font &font);
	void setFontSize(uint size);
	void setText(std::string text);
	
	sf::Vector2f getSize();
	void setSize(sf::Vector2f size, bool applyFixedSize = false, sf::Vector2<bool> fixedSize = sf::Vector2<bool>(false, false));

	virtual void setAction(std::function<void()> action);

	bool isMousePressed();
	bool wasButtonActivated();

	void setStyle(Parser &parser, std::string name = "Button");
	void setStyle(std::string style);

	void updateEvent(sf::Event &event);
	void update(sf::Vector2f mousePos, bool passiveMode = true);

	void goPassive();

protected:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	virtual void activateAction();
	void updatePushButton(bool changeMinimumSize = false);

protected:
	sf::Font *m_font;
	uint m_fontSize;

	bool m_customBodySize;

	sf::Color m_normalBody;
	sf::Color m_clickedBody;
	sf::Color m_hoverBody;

	sf::Color m_normalText;
	sf::Color m_clickedText;
	sf::Color m_hoverText;

	sf::Color m_normalBorder;
	sf::Color m_clickedBorder;
	sf::Color m_hoverBorder;

	float m_normalBorderThickness;
	float m_clickedBorderThickness;
	float m_hoverBorderThickness;

	float m_padding;

	sf::Text m_text;
	sf::RectangleShape m_body;

	bool m_mousePressed;
	bool m_actionDone;
	bool m_mouseInside;

	bool m_readyToSendMouse;

	bool m_actionIsSet;

	bool m_wasButtonActivated;

};

}

#endif