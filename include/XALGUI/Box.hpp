#ifndef XAL_BOX_HPP
#define XAL_BOX_HPP

#include <XALGUI/Global.hpp>

#include <XALGUI/Widget.hpp>
#include <XALGUI/Container.hpp>

namespace xal
{

template<class T = Widget::Ptr>
using PairContainer = Container<std::pair<T, std::string>>;

template<class T = Widget::Ptr>
class Box: public PairContainer<T>, public Widget
{
public:
	typedef std::shared_ptr<Box> Ptr;

	Box();

	void setPadding(float top, float bottom, float left, float right);
	void setPadding(float padding);
	void setObjectsSpace(float space);

	void updateObjects();

	void setSize(sf::Vector2f size, bool applyFixedSize = false, sf::Vector2<bool> fixedSize = sf::Vector2<bool>(false, false));
	sf::Vector2f getSize();

	T find(std::string name);

	void updateEvent(sf::Event &event);
	void update(sf::Vector2f mousePos, bool passiveMode = true);

	void setStyle(Parser &parser, std::string name);
	void setObjectsStyle(std::string style);

	void goPassive();

protected:
	bool sizeChanged();
	virtual void updateBox() = 0;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

protected:
	float m_paddingTop;
	float m_paddingBottom;
	float m_paddingLeft;
	float m_paddingRight;
	float m_padding;

	float m_objectsSpace;

	sf::Vector2f m_size;
	bool m_sizeChanged;

	bool m_objectsFullSize;

	bool m_useCustomTransformable;
	sf::Transformable m_customTransformable;

	std::string m_objectsStyle;
};

template<class T>
Box<T>::Box()
: m_paddingTop (0.f)
, m_paddingBottom (0.f)
, m_paddingLeft (0.f)
, m_paddingRight (0.f)
, m_padding (0.f)
, m_objectsSpace (0.f)
, m_size ({m_paddingLeft + m_paddingRight, m_paddingTop + m_paddingBottom})
, m_sizeChanged (false)
, m_objectsFullSize (true)
, m_useCustomTransformable (false)
{
	Widget::m_widgetType = "Box";
	this->m_size = {0.f, 0.f};
}

template<class T>
void Box<T>::setPadding(float top, float bottom, float left, float right)
{
	m_paddingTop = top;
	m_paddingBottom = bottom;
	m_paddingLeft = left;
	m_paddingRight = right;
}

template<class T>
void Box<T>::setPadding(float padding)
{
	m_padding = padding;
	setPadding(padding, padding, padding, padding);
	updateBox();
}

template<class T>
void Box<T>::setObjectsSpace(float space)
{
	m_objectsSpace = space;
}

template<class T>
void Box<T>::updateObjects()
{
	updateBox();
}

template<class T>
sf::Vector2f Box<T>::getSize()
{
	return m_size;
}

template<class T>
void Box<T>::setSize(sf::Vector2f size, bool applyFixedSize, sf::Vector2<bool> fixedSize)
{
	if (applyFixedSize) Widget::m_fixedSize = fixedSize;
	m_size = size;
	m_sizeChanged = true;
	updateBox();
}

template<class T>
T Box<T>::find(std::string name)
{
	for (auto it = this->m_objects.begin(); it != this->m_objects.end(); ++it)
	{
		if (it->second == name)
			return it->first;
	}
	return nullptr;
}

template<class T>
void Box<T>::updateEvent(sf::Event &event)
{
	for (auto it = this->m_objects.begin(); it != this->m_objects.end(); ++it)
	{
		it->first->updateEvent(event);
	}
}

template<class T>
void Box<T>::update(sf::Vector2f mousePos, bool passiveMode)
{
	if (!passiveMode)
	{
		for (auto it = this->m_objects.begin(); it != this->m_objects.end(); ++it)
		{
			it->first->update(mousePos - it->first->getPosition(), passiveMode);
		} 
	}
	else
	{
		bool finded = false;
		for (auto it = this->m_objects.begin(); it != this->m_objects.end(); ++it)
		{
			if (!finded && sf::FloatRect(it->first->getPosition().x, it->first->getPosition().y, it->first->getSize().x, it->first->getSize().y).contains(mousePos))
			{
				finded = true;
				it->first->update(mousePos - it->first->getPosition(), true);	
			}
			it->first->update(mousePos - it->first->getPosition(), false);
		}
	}
}

// template<class T>
// void Box<T>::setStyle(std::string style)
// {
// 	Parser parser(style);
// 	setStyle(parser, "");
// }

template<class T>
void Box<T>::setStyle(Parser &parser, std::string name)
{
	if (name != "")
	{
		if (!parser.enterObject(name))
		{
			// ERROR
		}
	}

	if (!parser.getValue("padding").empty())
	{
		setPadding(parser.getValue("padding").asFloat());
	}

	if (!parser.getValue("objectsSpacing").empty())
	{
		m_objectsSpace = parser.getValue("objectsSpacing").asFloat();
	}

	if (name != "")
	{
		parser.exitObject();
	}

	updateBox();
}

template<class T>
void Box<T>::setObjectsStyle(std::string style)
{
	m_objectsStyle = style;
	for (auto it = this->m_objects.begin(); it != this->m_objects.end(); ++it)
	{
		it->first->setStyle(style);
	}
}

template<class T>
void Box<T>::goPassive()
{
	for (auto it = this->m_objects.begin(); it != this->m_objects.end(); ++it)
	{
		it->first->goPassive();
	}
}

template<class T>
bool Box<T>::sizeChanged()
{
	if (m_sizeChanged)
	{
		m_sizeChanged = false;
		return true;
	}
	return false;
}

template<class T>
void Box<T>::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (m_useCustomTransformable) states.transform *= m_customTransformable.getTransform();
	else states.transform *= getTransform();
	for (std::size_t i=0; i<this->m_objects.size(); ++i)
	{
		target.draw(*this->m_objects[i].first, states);
	}
}

}

#endif