#ifndef XAL_Widget_HPP
#define XAL_Widget_HPP

#include <memory>
#include <SFML/Graphics.hpp>

#include <XALGUI/Parser.hpp>

namespace xal
{

class Widget: public sf::Drawable, public sf::Transformable
{
public:
	typedef std::shared_ptr<Widget> Ptr;

	Widget()
	: m_visible (true)
	, m_fixedSize ({false, false})
	, m_deleted (false)
	{}

	virtual void setSize(sf::Vector2f size, bool applyFixedSize = false, sf::Vector2<bool> fixedSize = sf::Vector2<bool>(false, false)) = 0; // Set the size of the widget
	virtual void setStyle(Parser &parser, std::string name) = 0; // Set the style of the widget
	virtual void setStyle(std::string style)
	{
		Parser parser(style);
		setStyle(parser, "");
	}
	void setVisible(bool visible) { m_visible = visible; } // Set if the widget is visible
	void setFixedSize(sf::Vector2<bool> fixedSize) { m_fixedSize = fixedSize; } // Set if the widget have fixed size ({width, height})

	virtual sf::Vector2f getSize() = 0; // Get the size of the widget
	sf::Vector2f getMinimumSize() const { return m_minimSize; } // Get the minimum size that the widget can take
	sf::Vector2<bool> isFixedSize() const { return m_fixedSize; }
	bool isVisible() const { return m_visible; }
	bool isDeleted() const { return m_deleted; }

	virtual void updateEvent(sf::Event &event) = 0;
	virtual void update(sf::Vector2f mousePos, bool passiveMode = true) = 0;

	virtual void goPassive() = 0;

	std::string getWidgetType() { return m_widgetType; }

protected:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;

protected:
	std::string m_widgetType;

	bool m_visible;

	sf::Vector2f m_minimSize;
	
	sf::Vector2<bool> m_fixedSize;

	bool m_deleted;

};

}

#endif