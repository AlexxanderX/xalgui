#ifndef MENUBARITEM_HPP
#define MENUBARITEM_HPP

#include <SFML/Graphics.hpp>

#include <XALGUI/PushButton.hpp>

namespace xal
{

namespace MenuBar
{

enum ActionType
{
	MenuBar = 0,
	Function = 1,
	NoAction
};

class SubBar;

class Item: public PushButton
{
public:
	typedef std::shared_ptr<Item> Ptr;

	static Ptr create(sf::Font &font, uint fontSize, std::string text);
	Item();

	void                    setAction(std::function<void()> action);
	void                    setAction(std::shared_ptr<SubBar> bar);
	std::shared_ptr<SubBar> setAction(std::string style = "");

	std::shared_ptr<SubBar> getActionMenuBar();

	void updateEvent(sf::Event &event);
	void update(sf::Vector2f mousePos, bool passiveMode = true);

	void setStyle(Parser &parser, std::string name = "MenuBarItem");
	void setStyle(std::string style);

	void goPassive();

	sf::FloatRect getActionBarGlobalBounds();

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void activateAction();
	void updateBar();

private:
	std::shared_ptr<SubBar> m_actionBar;
	sf::Uint8 m_actionType;
};

}

}

#endif