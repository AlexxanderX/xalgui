#ifndef XAL_VERTICALBOX_HPP
#define XAL_VERTICALBOX_HPP

#include <XALGUI/Box.hpp>

namespace xal
{

template<typename T = Widget::Ptr>
class VerticalBox: public Box<T>
{
public:
	typedef std::shared_ptr<VerticalBox> Ptr;

	template<typename R = Widget::Ptr>
	static Ptr create();

	VerticalBox();

	bool add(T object, std::string name = "");

	void setStyle(Parser &parser, std::string name = "VerticalBox");

private:
	void updateBox();

private:
	
};

template<typename T>
template<typename R>
typename VerticalBox<T>::Ptr VerticalBox<T>::create()
{
	auto ptr = std::make_shared<VerticalBox<R>>();

	return ptr;
}

template<typename T>
VerticalBox<T>::VerticalBox()
{
	Widget::m_widgetType = "VerticalBox";
}

template<typename T>
bool VerticalBox<T>::add(T object, std::string name)
{
	if(PairContainer<T>::add(std::pair<T, std::string>(object, name)))
	{
		if (this->m_objectsStyle != "")
			object->setStyle(this->m_objectsStyle);
		return true;
	}
	return false;
}

template<typename T>
void VerticalBox<T>::setStyle(Parser &parser, std::string name)
{
	if (name != "")
	{
		if (!parser.enterObject(name))
		{
			// ERROR
		}
	}

	if (name != "")
	{
		parser.exitObject();
	}
}

template<typename T>
void VerticalBox<T>::updateBox()
{
	if (this->m_objects.size() > 0)
	{
		/* DEBUG */ std::cout << "VerticalBox():updateBox() | m_objects.size() = " << this->m_objects.size() << " | m_size = (" << this->m_size.x << ", " << this->m_size.y << ")\n";

		bool boxIsFixed = (Widget::m_fixedSize.x || Widget::m_fixedSize.y);

		if (this->m_size.x == 0 && this->m_size.y == 0 && boxIsFixed)
		{
			/* WARNING */ std::cout << "XALGUI:Warning:Box has null&fixed size\n";
			return;
		}

		sf::Vector2f originalBoxSize = this->m_size;

		// The max size of the second axis of the box
		float boxObjectMaxWidth = this->m_size.x - 2*this->m_padding;
		for (auto it = this->m_objects.begin(); it != this->m_objects.end(); ++it)
		{
			float objectWidth = it->first->getMinimumSize().x;
			if (boxObjectMaxWidth < objectWidth)
			{
				// Checking if the box has the size fixed and if has fixed size, throw a warning, else enlarge the size of box
				bool boxIsNotFixedSize = !this->m_fixedSize.x;
				if (boxIsNotFixedSize)
				{
					boxObjectMaxWidth = objectWidth;
					this->m_size.x = objectWidth + 2*this->m_padding;
				}
				else
					/* WARNING */ std::cout << "XALGUI:Warning:The size of objects is too big for the box!(:" << __LINE__ << ")\n";
			}
			else
			{
				it->first->setSize({boxObjectMaxWidth, it->first->getSize().y});
			}
		}

		/* DEBUG */ std::cout << "\tboxObjectMaxWidth = " << boxObjectMaxWidth << "\n";

		// Calculating the empty size of the box after decrease the size of the fixed objects and the padding
		float emptySize = this->m_size.y - 2*this->m_padding;
		//* DEBUG */ std::cout << "\temptySize = " << emptySize << "\n";

		// Storing the objects who does not have the size fixed
		std::vector<int> notFixedSizeObjects;
		for (std::size_t i=0; i<this->m_objects.size(); ++i)
		{
			bool objectIsFixedSize = this->m_objects[i].first->isFixedSize().y;
			/* DEBUG */ std::cout << "\ti = " << i << " | objectIsFixedSize = " << objectIsFixedSize << "\n";
			if (objectIsFixedSize)
			{
				emptySize -= this->m_objects[i].first->getSize().y;
				//* DEBUG */ std::cout << "\t\temptySize = " << emptySize << "\n";
			}
			else
				notFixedSizeObjects.push_back(i);

			if (i != this->m_objects.size()-1)
				emptySize -= this->m_objectsSpace;
		}

		/* DEBUG */ std::cout << "\temptySize = " << emptySize << "; notFixedSizeObjects.size() = " << notFixedSizeObjects.size() << "\n";

		// Checking if the size of the box is too small for the objects
		if (emptySize <= 0)
		{
			if (boxIsFixed)
				/* WARNING */ std::cout << "XALGUI:Warning:The size of objects is too big for the box!(:" << __LINE__ << ")\n";
			else
			{
				emptySize *= -1;

				if (!notFixedSizeObjects.empty())
				{
					for (std::size_t i=0; i<notFixedSizeObjects.size(); ++i)
					{
						this->m_objects[notFixedSizeObjects[i]].first->setSize({boxObjectMaxWidth, this->m_objects[notFixedSizeObjects[i]].first->getMinimumSize().y});
						emptySize += this->m_objects[notFixedSizeObjects[i]].first->getMinimumSize().y;
					}
				}

				this->m_size.y = emptySize;
			}
		}
		else if (!notFixedSizeObjects.empty())
		{
			// The prototype size of the other objects
			float itemSize = emptySize / static_cast<float>(notFixedSizeObjects.size());

			// Storing the objects who have the minim size bigger then the prototype size
			std::vector<int> aboveMinimSizeObjects;
			for (std::size_t i=0; i<notFixedSizeObjects.size(); ++i)
			{
				float objectHeight = this->m_objects[i].first->getMinimumSize().y;
				if (itemSize < objectHeight)
				{
					emptySize -= objectHeight;
					aboveMinimSizeObjects.push_back(notFixedSizeObjects[i]);
					notFixedSizeObjects.erase(notFixedSizeObjects.begin() + i);

					itemSize = emptySize / static_cast<float>(notFixedSizeObjects.size());
					i = 0;
				}
			}

			//* DEBUG */ std::cout << "\tnotFixedSizeObjects.size() = " << notFixedSizeObjects.size() << "\n";

			if (emptySize < 0 && this->m_fixedSize.y)
			{
				/* WARNING */ std::cout << "XALGUI:Warning:The size of objects is too big for the box!(:" << __LINE__ << ")\n";
			}
			else
			{
				for (std::size_t i=0; i<aboveMinimSizeObjects.size(); ++i)
				{
					this->m_size.y += this->m_objects[aboveMinimSizeObjects[i]].first->getMinimumSize().y;
					this->m_objects[aboveMinimSizeObjects[i]].first->setSize({boxObjectMaxWidth, this->m_objects[aboveMinimSizeObjects[i]].first->getMinimumSize().y});
				}

				this->m_size.y += itemSize * notFixedSizeObjects.size();

				for (std::size_t i=0; i<notFixedSizeObjects.size(); ++i)
				{
					this->m_objects[notFixedSizeObjects[i]].first->setSize({boxObjectMaxWidth, itemSize});
				}
			}
		}

		if (originalBoxSize.x != this->m_size.x || originalBoxSize.y != this->m_size.y)
			this->m_sizeChanged = true;

		/* DEBUG */ std::cout << "\tm_sizeChanged = " << this->m_sizeChanged << "\n";

		this->m_objects[0].first->setPosition(this->m_padding, this->m_padding);
		for (auto it = this->m_objects.begin() + 1; it != this->m_objects.end(); ++it)
		{
			auto previous = std::prev(it);
			//* DEBUG */ std::cout << "it = " << &(*it) << " | (" << x << ", " << y << ")\n";
			it->first->setPosition(this->m_padding, previous->first->getPosition().y + previous->first->getSize().y + this->m_objectsSpace);
		}

		//* NEED REVIEW */ this->m_size = this->m_objects[this->m_objects.size()-1].first->getPosition() + this->m_objects[this->m_objects.size()-1].first->getSize() + sf::Vector2f(this->m_padding, this->m_padding);
		/* DEBUG */ std::cout << "VerticalBox():updateBox() | COMPLETED | m_size = (" << this->m_size.x << ", " << this->m_size.y << ")\n";
	}
}

}

#endif