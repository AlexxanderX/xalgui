#ifndef XAL_MENUSUBBAR_HPP
#define XAL_MENUSUBBAR_HPP

#include <XALGUI/VerticalBox.hpp>

namespace xal
{

namespace MenuBar
{

class Item;

class SubBar: public VerticalBox<std::shared_ptr<Item>>
{
public:
	typedef std::shared_ptr<SubBar> Ptr;
	static Ptr create(sf::Font &font, unsigned int textSize);

	SubBar();

	void setFont(sf::Font &font);
	void setTextSize(const unsigned int size);
	void setStyle(Parser &parser, std::string name = "MenuBar");
	void setStyle(std::string style);

	sf::FloatRect getGlobalBounds();
	std::vector<sf::FloatRect> getActionBarsGlobalBounds();
	bool itemWasPressed();

	std::shared_ptr<Item> add(std::string name, std::string style = "");
	void updateObjects();

	void update(sf::Vector2f mousePos, bool passiveMode = true);
private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void updateSubBar();
private:
	sf::Font *m_font;
	unsigned int m_textSize;

	sf::RectangleShape m_body;

	bool m_itemWasPressed;

};

}

}

#endif