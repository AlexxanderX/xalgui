#ifndef XAL_BUTTON_HPP
#define XAL_BUTTON_HPP

#include <XALGUI/Widget.hpp>

namespace xal
{

class Button: public Widget
{
public:
	Button()
	{}

	virtual void updateEvent(sf::Event &event) = 0;
	virtual void update(sf::Vector2f mousePos, bool passiveMode = true) = 0;

	virtual sf::Vector2f getSize() = 0;
	virtual void setSize(sf::Vector2f size, bool applyFixedSize = false, sf::Vector2<bool> fixedSize = sf::Vector2<bool>(false, false)) = 0;

	virtual void setStyle(Parser &parser, std::string name) = 0;

	virtual void setAction(std::function<void()> action) = 0;

	virtual void goPassive() = 0;

protected:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;

protected:
	std::function<void()> m_action;
};

}

#endif