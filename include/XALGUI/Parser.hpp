#ifndef XAL_PARSER_HPP
#define XAL_PARSER_HPP

#include <XALGUI/Global.hpp>

#include <memory>
#include <fstream>
#include <json/json.h>

#include <SFML/Graphics.hpp>

namespace xal
{

enum ValueType
{
	Int = 0,
	Float
};

class Parser
{
public:
	Parser();
	Parser(std::string style, bool isFilePath = false);

	void setStyle(std::string style);
	void setStyleFile(std::string filePath);

	bool enterObject(std::string name);
	bool exitObject();
	void exitToRoot();

	Json::Value getValue(std::string name);

	sf::Color getColor(std::string name);

	std::string getStyle(std::string name);

private:

private:
	Json::Value m_root;

	std::vector<Json::Value> m_temporaryValues;
	
};

}

#endif