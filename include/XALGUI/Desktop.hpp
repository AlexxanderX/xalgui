#ifndef XAL_DESKTOP_HPP
#define XAL_DESKTOP_HPP

#include "Global.hpp"

#include <SFML/Graphics.hpp>

#include <vector>

#include <XALGUI/Widget.hpp>
#include <XALGUI/Container.hpp>
#include <XALGUI/MenuBar.hpp>
#include <XALGUI/MenuBarItem.hpp>
#include <XALGUI/HorizontalBox.hpp>

namespace xal
{

class Desktop: public sf::Drawable
{
public:
	Desktop();

	void handleEvents(sf::Event &event, sf::Vector2f mousePos);
	void update();

	void add(MenuBar::Bar::Ptr object);
	void add(Widget::Ptr object);

	void moveToFront(Widget::Ptr object);

private:
	template<typename T>
	void updateContainer(std::vector<T> &objects);

	template<typename T>
	void updateActiveObject(T object);

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	bool objectContainsMousePosition(MenuBar::Bar::Ptr object);
	bool objectContainsMousePosition(Widget::Ptr object);

private:
	Widget::Ptr m_activeWidget;
	MenuBar::Bar::Ptr m_activeMenuBar;
	int m_activeWidgetType;

	bool m_activeObjectContainsMouse;

	std::vector<Widget::Ptr> m_widgets;
	std::vector<MenuBar::Bar::Ptr> m_menuBars;
	
	sf::Vector2f m_mousePos;
};

template<typename T>
void Desktop::updateContainer(std::vector<T> &objects)
{
	for (auto it = objects.begin() ; it != objects.end(); ++it)
	{
		if ((*it)->isDeleted())
		{
			it = objects.erase(it);
			if (it == objects.end()) break;
		}

		if (!m_activeObjectContainsMouse)
		{
			sf::Vector2f translatedPos = m_mousePos - (*it)->getPosition();
			if ((*it)->isVisible())
			{
				(*it)->update(translatedPos);
			}
		}
	}
}

template<typename T>
void Desktop::updateActiveObject(T object)
{
	if (object->isDeleted())
	{
		object = nullptr;
		m_activeWidgetType = -1;
		m_activeObjectContainsMouse = false;
		return;
	}

	object->update(m_mousePos - object->getPosition(), false);
}

}

#endif