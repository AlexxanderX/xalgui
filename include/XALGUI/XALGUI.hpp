#ifndef XAL_XALGUI_HPP
#define XAL_XALGUI_HPP

#include <XALGUI/Global.hpp>

#include <XALGUI/Box.hpp>
//#include <XALGUI/Button.hpp>
//#include <XALGUI/Container.hpp>
#include <XALGUI/Parser.hpp>
#include <XALGUI/Desktop.hpp>
#include <XALGUI/LineEdit.hpp>
#include <XALGUI/MenuBar.hpp>
#include <XALGUI/MenuBarItem.hpp>
#include <XALGUI/MenuSubBar.hpp>
//#include <XALGUI/Widget.hpp>
#include <XALGUI/Window.hpp>

#endif