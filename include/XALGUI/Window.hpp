#ifndef XAL_WINDOW_HPP
#define XAL_WINDOW_HPP

#include <SFML/Graphics.hpp>

#include <XALGUI/Widget.hpp>
#include <XALGUI/Box.hpp>
#include <XALGUI/PushButton.hpp>

namespace xal
{

class Window: public Widget
{
public:
	typedef std::shared_ptr<Window> Ptr;

	static Ptr create(sf::Font &font, std::string title);
	Window();

	void setFont(sf::Font &font);
	void setTitle(std::string title);
	void setStyle(Parser &parser, std::string name = "Window");
	void setStyle(std::string style);
	void setSize(sf::Vector2f size, bool applyFixedSize = false, sf::Vector2<bool> fixedSize = sf::Vector2<bool>(false, false));
	void setBox(Box<Widget::Ptr>::Ptr box);

	sf::Vector2f getSize();

	void updateEvent(sf::Event &event);
	void update(sf::Vector2f mousePos, bool passiveMode = true);

	void closeAction(std::function<void()> action);
	void close();

	void goPassive();

protected:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void updateWindow();
	
protected:
	sf::RectangleShape m_titleBar;
	sf::RectangleShape m_body;

	xal::PushButton::Ptr m_closeButton;

	sf::Font *m_font;
	sf::Text m_title;

	bool m_moveActive;
	sf::Vector2f m_moveOldPos;

	Box<Widget::Ptr>::Ptr m_box;

};

}

#endif