#ifndef MENUBAR_HPP
#define MENUBAR_HPP

#include <SFML/Graphics.hpp>

#include <vector>
#include <utility>

#include <XALGUI/Widget.hpp>
#include <XALGUI/HorizontalBox.hpp>
#include <XALGUI/MenuBarItem.hpp>

namespace xal
{

namespace MenuBar
{

class Bar: public HorizontalBox<Item::Ptr>
{
public:
	typedef std::shared_ptr<Bar> Ptr;

	static Ptr create(sf::Font &font, unsigned int textSize, float width);
	Bar();

	void setFont(sf::Font &font);
	void setTextSize(const unsigned int size);
	void setStyle(Parser &parser, std::string name = "MenuBar");
	void setStyle(std::string style);

	std::vector<sf::FloatRect> getActionBarsGlobalBounds();
	bool itemWasPressed();

	Item::Ptr add(std::string name, std::string style = "");
	void updateObjects();

	void update(sf::Vector2f mousePos, bool passiveMode = true);
	
private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void updateBar();

private:
	sf::Font *m_font;
	unsigned int m_textSize;

	sf::RectangleShape m_body;

	bool m_itemWasPressed;
};

}

}

#endif