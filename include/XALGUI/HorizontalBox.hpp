#ifndef XAL_HORIZONTALBOX_HPP
#define XAL_HORIZONTALBOX_HPP

#include <XALGUI/Box.hpp>

namespace xal
{

template<typename T = Widget::Ptr>
class HorizontalBox: public Box<T>
{
public:
	typedef std::shared_ptr<HorizontalBox> Ptr;

	static Ptr create();

	HorizontalBox();

	bool add(T object, std::string name = "");

	void setStyle(Parser &parser, std::string name = "HorizontalBox");

private:
	void updateBox();

private:

	
};

template<typename T>
typename HorizontalBox<T>::Ptr HorizontalBox<T>::create()
{
	auto ptr = std::make_shared<HorizontalBox<T>>();

	return ptr;
}

template<typename T>
HorizontalBox<T>::HorizontalBox()
{
	Widget::m_widgetType = "HorizontalBox";
}

template<typename T>
bool HorizontalBox<T>::add(T object, std::string name)
{
	if(PairContainer<T>::add(std::pair<T, std::string>(object, name)))
	{
		if (this->m_objectsStyle != "")
			object->setStyle(this->m_objectsStyle);
		//* DEBUG */ std::cout << "HorizontalBox():add() | object:isFixedSize = " << this->m_objects[this->m_objects.size()-1].first->isFixedSize().x << "\n"; 
		return true;
	}
	return false;
}

template<typename T>
void HorizontalBox<T>::setStyle(Parser &parser, std::string name)
{
	if (name != "")
	{
		if (!parser.enterObject(name))
		{
			// ERROR
		}
	}

	if (name != "")
	{
		parser.exitObject();
	}
}

template<typename T>
void HorizontalBox<T>::updateBox()
{
	if (this->m_objects.size() > 0)
	{
		/* DEBUG */ std::cout << "HorizontalBox():updateBox() | m_objects.size() = " << this->m_objects.size() << " | m_size = (" << this->m_size.x << ", " << this->m_size.y << "\n";
		// if (this->m_objects.size() == 1)
		// {
		// 	if (this->m_size.x != 0 && this->m_size.y != 0)
		// 	{
		// 		if ((this->m_size.x < this->m_objects[0].first->getSize().x) || (this->m_size.y < this->m_objects[0].first->getSize().y))
		// 		{
		// 			/* WARNING */ std::cout << "XALGUI:Warning:The size of object is too big for the box!\n";
		// 		}
		// 		else
		// 		{
		// 			this->m_objects[0].first->setSize(this->m_size - sf::Vector2f(2*this->m_padding, 2*this->m_padding));
		// 			this->m_objects[0].first->setPosition(this->m_padding, this->m_padding);
		// 		}
		// 	}
		// 	else
		// 	{
		// 		this->m_size = sf::Vector2f(2*this->m_padding, 2*this->m_padding) + this->m_objects[0].first->getSize();
		// 	}
		// 	return;
		// }

		bool boxIsFixed = (Widget::m_fixedSize.x || Widget::m_fixedSize.y);

		if (this->m_size.x == 0 && this->m_size.y == 0 && boxIsFixed)
		{
			/* WARNING */ std::cout << "XALGUI:Warning:Box has null&fixed size\n";
			return;
		}

		/// workingValue is a transitive variable for knowing on what to work by the orientation of the box

		// Checking the second axis of the object to see if is bigger than the box

		/// TODO: Verific daca e constanta(fixata) secondAxis a obiectelor. Sa compar secondAxis cu marimea minima a obiectelor?

		// The max size of the second axis of the box
		float boxObjectMaxHeight = this->m_size.y - 2*this->m_padding;
		for (auto it = this->m_objects.begin(); it != this->m_objects.end(); ++it)
		{
			float objectHeight = it->first->getMinimumSize().y;
			if (boxObjectMaxHeight < objectHeight)
			{
				// Checking if the box has the size fixed and if has fixed size, throw a warning, else enlarge the size of box
				bool boxIsNotFixedSize = !this->m_fixedSize.y;
				if (boxIsNotFixedSize)
				{
					boxObjectMaxHeight = objectHeight;
					this->m_size.y = objectHeight + 2*this->m_padding;
					this->m_sizeChanged = true;
				}
				else
					/* WARNING */ std::cout << "XALGUI:Warning:The size of objects is too big for the box!(:" << __LINE__ << ")\n";
			}
			else
			{
				it->first->setSize({it->first->getSize().x, boxObjectMaxHeight});
			}
		}

		// Calculating the empty size of the box after decrease the size of the fixed objects and the padding
		float emptySize = this->m_size.x - 2*this->m_padding;

		// Storing the objects who does not have the size fixed
		std::vector<int> notFixedSizeObjects;
		for (std::size_t i=0; i<this->m_objects.size(); ++i)
		{
			bool objectIsFixedSize = this->m_objects[i].first->isFixedSize().x;
			/* DEBUG */ std::cout << "\ti = " << i << " | objectIsFixedSize = " << objectIsFixedSize << "\n";
			if (objectIsFixedSize)
				emptySize -= this->m_objects[i].first->getSize().x;
			else
				notFixedSizeObjects.push_back(i);

			if (i != this->m_objects.size()-1)
				emptySize -= this->m_objectsSpace;
		}

		// Checking if the size of the box is too small for the objects
		if (emptySize <= 0)
		{
			if (boxIsFixed)
				/* WARNING */ std::cout << "XALGUI:Warning:The size of objects is too big for the box!(:" << __LINE__ << ")\n";
			else
			{
				emptySize *= -1;

				if (!notFixedSizeObjects.empty())
				{
					for (std::size_t i=0; i<notFixedSizeObjects.size(); ++i)
					{
						this->m_objects[notFixedSizeObjects[i]].first->setSize({this->m_objects[notFixedSizeObjects[i]].first->getMinimumSize().x, boxObjectMaxHeight});
						emptySize += this->m_objects[notFixedSizeObjects[i]].first->getMinimumSize().x;
					}
				}

				this->m_size.x = emptySize;
				this->m_sizeChanged = true;
			}
		}
		else if (!notFixedSizeObjects.empty())
		{
			// The prototype size of the other objects
			float itemSize = emptySize / static_cast<float>(notFixedSizeObjects.size());

			// Storing the objects who have the minim size bigger then the prototype size
			std::vector<int> belowMinimSizeObjects;
			for (std::size_t i=0; i<notFixedSizeObjects.size(); ++i)
			{
				float objectWidth = this->m_objects[i].first->getSize().x;
				if (itemSize < objectWidth)
				{
					emptySize -= objectWidth;
					belowMinimSizeObjects.push_back(notFixedSizeObjects[i]);
					notFixedSizeObjects.erase(notFixedSizeObjects.begin() + i);

					itemSize = emptySize / static_cast<float>(notFixedSizeObjects.size());
					i = 0;
				}
			}

			bool boxIsFixedSize = this->m_fixedSize.x;
			if (emptySize < 0 && boxIsFixedSize)
			{
				/* WARNING */ std::cout << "XALGUI:Warning:The size of objects is too big for the box!(:" << __LINE__ << ")\n";
			}
			else if (!belowMinimSizeObjects.empty())
			{
				this->m_size.x += -1*emptySize;
				this->m_sizeChanged = true;
				
				for (std::size_t i=0; i<belowMinimSizeObjects.size(); ++i)
				{
					this->m_objects[belowMinimSizeObjects[i]].first->setSize({this->m_objects[belowMinimSizeObjects[i]].first->getSize().x, boxObjectMaxHeight});
				}
			}
			else
			{
				for (std::size_t i=0; i<notFixedSizeObjects.size(); ++i)
				{
					this->m_objects[notFixedSizeObjects[i]].first->setSize({itemSize, boxObjectMaxHeight});
				}
			}
		}

		this->m_objects[0].first->setPosition(this->m_padding, this->m_padding);
		for (auto it = this->m_objects.begin() + 1; it != this->m_objects.end(); ++it)
		{
			auto previous = std::prev(it);
			//* DEBUG */ std::cout << "it = " << &(*it) << " | (" << x << ", " << y << ")\n";
			it->first->setPosition(previous->first->getPosition().x + previous->first->getSize().x + this->m_objectsSpace, previous->first->getPosition().y);
		}

		//* NEED REVIEW */ this->m_size = this->m_objects[this->m_objects.size()-1].first->getPosition() + this->m_objects[this->m_objects.size()-1].first->getSize() + sf::Vector2f(this->m_padding, this->m_padding);
		/* DEBUG */ std::cout << "HorizontalBox():updateBox() | COMPLETED | m_size = (" << this->m_size.x << ", " << this->m_size.y << ")\n";
	}
}

}

#endif