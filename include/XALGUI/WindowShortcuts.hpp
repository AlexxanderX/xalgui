#ifndef XAL_WINDOWSHORTCUTS_HPP
#define XAL_WINDOWSHORTCUTS_HPP

#include <XALGUI/Window.hpp>
#include <XALGUI/PushButton.hpp>
#include <XALGUI/LineEdit.hpp>

namespace xal
{

inline Window::Ptr createInputWindow(sf::Font &font, std::string title, std::string description, std::function<void()> returnAction, Parser &parser, std::string styleName);
{
	parser.enterObject(styleName);

	auto window = Window::create(font, title);
	window->
}

}

#endif