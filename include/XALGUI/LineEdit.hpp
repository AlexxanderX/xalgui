#ifndef XAL_LINEEDIT_HPP
#define XAL_LINEEDIT_HPP

#include <XALGUI/Widget.hpp>

namespace xal
{

class LineEdit: public Widget
{
public:
	typedef std::shared_ptr<LineEdit> Ptr;
	static Ptr create(sf::Font &font, unsigned int characterSize, std::string text = "");

	LineEdit();

	void setFont(sf::Font &font);
	void setCharacterSize(unsigned int size);
	void setText(std::string text);
	void setSize(sf::Vector2f size, bool applyFixedSize = false, sf::Vector2<bool> fixedSize = sf::Vector2<bool>(false, false));
	void setStyle(Parser &parser, std::string name = "LineEdit");
	void setStyle(std::string style);

	sf::Vector2f getSize();

	void updateEvent(sf::Event &event);
	void update(sf::Vector2f mousePos, bool passiveMode = true);

	void goPassive(); 
private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void updateLineEdit();

	float getFontHeight(unsigned int size);

private:
	sf::Font *m_font;
	sf::Text m_text;

	float m_paddingLeft;
	float m_paddingRight;
	float m_paddingTop;
	float m_paddingBottom;

	bool m_mouseInside;
	bool m_mousePressed;
	bool m_cursor;
	bool m_cursorShow;

	sf::Clock m_clock;

	sf::RectangleShape m_body;
	sf::RectangleShape m_caret;
};

}

#endif