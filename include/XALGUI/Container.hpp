#ifndef XAL_CONTAINER_HPP
#define XAL_CONTAINER_HPP

#include <vector>
#include <algorithm>

namespace xal
{

class Desktop;

template<typename T>
class Container
{
public:
	Container() {}

	bool add(T object);
	bool moveToFront(T object);
	//void remove(T object);

protected:

protected:
	std::vector<T> m_objects;
};

template<typename T>
bool Container<T>::add(T object)
{
	//std::cout << &object << " - Container\n";

	typename std::vector<T>::iterator it = std::find(m_objects.begin(), m_objects.end(), object);
	if (it == m_objects.end())
	{
		m_objects.push_back(object);
		return true;
	}
	return false;
}

template<typename T>
bool Container<T>::moveToFront(T object)
{
	// TODO: Oare trebuie aduagat verificare daca obiectul este in container?
	typename std::vector<T>::iterator element = std::find(m_objects.begin(), m_objects.end(), object);
   	m_objects.erase(element);
   	m_objects.push_back(object);
    return true;
}

}

#endif